<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="post">
        @csrf
		<label for="">First name</label><br><br>
		<input type="name"name="name_depan"><br><br>
		<label for="">Last name</label><br><br>
		<input type="name" lastname="name_belakang">
				<br><br>
		<label for="">Gender:</label><br><br>
		<input type="radio" id="male" name="Gender"><label>Male</label><br>
		<input type="radio" id="female" name="Gender"><label>female</label><br>
		<input type="radio" id="Other" name="Gender"><label>Other</label>
				<br><br>
		<label>Nationality:</label><br><br>
			<select name="Nationality">
				<option value="Indonesia">Indonesia</option>
				<option value="inggris">inggris</option>
				<option value="Jerman">Jerman</option>
			</select>
				<br><br>
		<label>Languange Spoken:</label><br><br>
		<input type="checkbox"><label>Bahasa Indonesia</label><br>
		<input type="checkbox"><label>English</label><br>
		<input type="checkbox"><label>Other</label>
				<br><br>
		<label>Bio:</label><br><br>
		<textarea name="Bio"cols="30" rows="10"></textarea><br>
		<input type="Submit">

	</form>
</body>
</html>